layui.use(['layer', 'form', 'table', 'ztree', 'laydate', 'admin', 'ax'], function () {
    var layer = layui.layer;
    var form = layui.form;
    var table = layui.table;
    var $ZTree = layui.ztree;
    var $ax = layui.ax;
    var laydate = layui.laydate;
    var admin = layui.admin;

    var Channel = {
        tableId: "channelTable",    //表格id
        condition: {}
    };

    /**
     * 初始化表格的列
     */
    Channel.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'channelId', sort: true, width: 150, title: '链接标识'},
            {field: 'ip', sort: true, width: 150, title: 'IP'},
            {field: 'port', sort: true, width: 150, title: '端口'},
            {field: 'messageNum', sort: true, width: 100, title: '数据量'},
            {field: 'time', sort: true, title: '链接时间'},
            {align: 'center', toolbar: '#tableBar', title: '操作', minWidth: 100}
        ]];
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + Channel.tableId,
        url: Feng.ctxPath + '/channel/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: Channel.initColumn()
    });

    /**
     * 点击查询按钮
     */
    Channel.search = function () {
        table.reload(Channel.tableId, {where: null});
    };

    /**
     * 定时任务
     */
    var ref = setInterval(function () {
        //刷新表格
        table.reload(Channel.tableId);
    }, 20000);

    /**
     * 数据详情
     */
    Channel.onDetail = function (data) {
        admin.putTempData('formOk', false);
        top.layui.admin.open({
            type: 2,
            title: '数据详情（【id：' + data.channelId + '】 【ip：' + data.ip + '】 【port：' + data.port + '】【数据量：' + data.messageNum + '】）',
            area: ['70%', '85%'],    //宽高-->
            content: Feng.ctxPath + '/channel/info/?channelId=' + data.channelId,
            end: function () {
                admin.getTempData('formOk') && table.reload(Channel.tableId);
            }
        });
    };

    /**
     * 发送数据
     *
     */
    Channel.onSend = function (data) {
        admin.putTempData('formOk', false);
        top.layui.admin.open({
            type: 2,
            title: '传输数据',
            content: Feng.ctxPath + '/channel/send/?channelId=' + data.channelId,
            end: function () {
                admin.getTempData('formOk') && table.reload(Channel.tableId);
            }
        });
    };

    /**
     * 删除
     *
     */
    Channel.onDelete = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/channel/delete", function () {
                table.reload(Channel.tableId);
                Feng.success("删除成功!");
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("channelId", data.channelId);
            ajax.start();
        };
        Feng.confirm("是否删除链接 " + data.channelId + "?", operation);
    };

    /**
     * 导出excel按钮
     */
    Channel.exportExcel = function () {
        var checkRows = table.checkStatus(Channel.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    };

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        Channel.search();
    });

    // 导出excel
    $('#btnExp').click(function () {
        Channel.exportExcel();
    });

    // 工具条点击事件
    table.on('tool(' + Channel.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'send') {
            Channel.onSend(data);
        } else if (layEvent === 'delete') {
            Channel.onDelete(data);
        } else if (layEvent === 'detail') {
            Channel.onDetail(data);
        }
    });
});
