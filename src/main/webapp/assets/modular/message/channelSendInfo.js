layui.use(['layer', 'form', 'table', 'ztree', 'laydate', 'admin', 'ax'], function () {
    var layer = layui.layer;
    var form = layui.form;
    var table = layui.table;
    var $ZTree = layui.ztree;
    var $ax = layui.ax;
    var laydate = layui.laydate;
    var admin = layui.admin;

    /**
     * 订单
     */
    var SendInfo = {
        tableId: "sendInfoTable",   //表格id
        condition: {}
    };

    /**
     * 初始化表格的列
     */
    SendInfo.initColumn = function () {
        return [[
            {field: 'ip', sort: true, title: 'IP'},
            {field: 'port', sort: true, title: '端口'},
            {field: 'data', sort: true, title: '报文'},
            {field: 'time', sort: true, title: '发送时间'},
            {field: 'type', sort: true, title: '属性'}
        ]];
    };


    // 渲染表格
    var tableResult = table.render({
        elem: '#' + SendInfo.tableId,
        url: Feng.ctxPath + '/channel/send/info?channelId=' +  $("#channelId").val(),
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: SendInfo.initColumn()
    });
});
