layui.use(['layer', 'form', 'table', 'ztree', 'laydate', 'admin', 'ax'], function () {
    var layer = layui.layer;
    var form = layui.form;
    var table = layui.table;
    var $ZTree = layui.ztree;
    var $ax = layui.ax;
    var laydate = layui.laydate;
    var admin = layui.admin;

    var Websocket = {
        tableId: "websocketTable",    //表格id
        condition: {}
    };

    /**
     * 初始化表格的列
     */
    Websocket.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'sessionId', sort: true, width: 80, title: 'key'},
            {field: 'explain', sort: true, width: 150, title: '推送说明'},
            {field: 'value', sort: true, width: 600, title: 'websocket连接值'},
            {field: 'ip', sort: true, width: 150, title: 'IP'},
            {field: 'port', sort: true, width: 80, title: '端口'},
            {field: 'messageNum', sort: true, width: 120, title: '推送次数'},
            {field: 'time', sort: true, width: 180, title: '连接时间'},
            {field: 'onLineTime', sort: true, width: 140, title: '在线时长/分钟'},
            {align: 'center', toolbar: '#tableBar', title: '操作', minWidth: 150}
        ]];
    };

    // 渲染表格
    var tableResult = table.render({
        elem: '#' + Websocket.tableId,
        url: Feng.ctxPath + '/websocket/list',
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: Websocket.initColumn()
    });

    /**
     * 点击查询按钮
     */
    Websocket.search = function () {
        table.reload(Websocket.tableId, {where: null});
    };

    /**
     * 定时任务 定时刷新表格
     */
    var ref = setInterval(function () {
        table.reload(Websocket.tableId);
    }, 30000);

    /**
     * 数据详情
     */
    Websocket.onDetail = function (data) {
        admin.putTempData('formOk', false);
        top.layui.admin.open({
            type: 2,
            title: '数据详情（【id：' + data.sessionId + '】 【ip：' + data.ip + '】 【port：' + data.port + '】【数据量：' + data.messageNum + '】）',
            area: ['85%', '85%'],    //宽高-->
            content: Feng.ctxPath + '/websocket/info/?sessionId=' + data.sessionId,
            end: function () {
                admin.getTempData('formOk') && table.reload(Websocket.tableId);
            }
        });
    };

    /**
     * 发送数据
     *
     */
    Websocket.onSend = function (data) {
        admin.putTempData('formOk', false);
        top.layui.admin.open({
            type: 2,
            title: '传输数据',
            area: ['500px', '400px'],
            content: Feng.ctxPath + '/websocket/send/?sessionId=' + data.sessionId + "&value=" + data.value,
            end: function () {
                admin.getTempData('formOk') && table.reload(Websocket.tableId);
            }
        });
    };

    /**
     * 删除
     *
     */
    Websocket.onDelete = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/websocket/delete", function () {
                table.reload(Websocket.tableId);
                Feng.success("删除成功!");
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("sessionId", data.sessionId);
            ajax.start();
        };
        Feng.confirm("是否删除链接 " + data.sessionId + "?", operation);
    };

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        Websocket.search();
    });

    // 工具条点击事件
    table.on('tool(' + Websocket.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;

        if (layEvent === 'send') {
            Websocket.onSend(data);
        } else if (layEvent === 'delete') {
            Websocket.onDelete(data);
        } else if (layEvent === 'detail') {
            Websocket.onDetail(data);
        }
    });
});
