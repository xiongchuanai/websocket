layui.use(['layer', 'form', 'table', 'ztree', 'laydate', 'admin', 'ax'], function () {
    var layer = layui.layer;
    var form = layui.form;
    var table = layui.table;
    var $ZTree = layui.ztree;
    var $ax = layui.ax;
    var laydate = layui.laydate;
    var admin = layui.admin;

    var SendInfo = {
        tableId: "sendInfoTable",   //表格id
        condition: {}
    };

    /**
     * 初始化表格的列
     */
    SendInfo.initColumn = function () {
        return [[
            {field: 'value', sort: true, width: 450, title: '对应的value'},
            {field: 'data', sort: true, title: '数据'},
            {field: 'time', sort: true, width: 180, title: '发送时间'},
            {field: 'isSend', sort: true, width: 130, title: '是否发送成功'}
        ]];
    };


    // 渲染表格
    var tableResult = table.render({
        elem: '#' + SendInfo.tableId,
        url: Feng.ctxPath + '/websocket/send/info?sessionId=' +  $("#sessionId").val(),
        page: true,
        height: "full-158",
        cellMinWidth: 100,
        cols: SendInfo.initColumn()
    });
});
