package com.xc.config.async;

import com.xc.constant.AsyncConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * 配置异步启动项
 *
 * @author XIONGCHUAN
 * @time 2019年5月30日 上午9:56:16
 * @描述
 */
@Slf4j
@Configuration
public class AsyncConfig implements AsyncConfigurer {

    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        //线程池维护线程所允许的空闲时间
        threadPoolTaskExecutor.setKeepAliveSeconds(AsyncConstants.KEEP_ALIVE_SECONDS);
        // 线程池维护核心线程的数量
        threadPoolTaskExecutor.setCorePoolSize(AsyncConstants.CORE_THREAD_NUM);
        // 线程池维护线程的最大数量
        threadPoolTaskExecutor.setMaxPoolSize(AsyncConstants.MAX_THREAD_NUM);
        // 缓存队列
        threadPoolTaskExecutor.setQueueCapacity(AsyncConstants.CACHE_NUM);
        // 线程名前缀,方便排查问题
        threadPoolTaskExecutor.setThreadNamePrefix(AsyncConstants.PREFIX_NAME);
        threadPoolTaskExecutor.initialize();
        log.info("初始化异步项配置完成");
        log.info("核心线程数：" + AsyncConstants.CORE_THREAD_NUM + " 最大线程数：" + AsyncConstants.MAX_THREAD_NUM + " 缓存队列数：" +
                AsyncConstants.CACHE_NUM + " 线程前缀：" + AsyncConstants.PREFIX_NAME);
        return threadPoolTaskExecutor;
    }

    /**
     * 异常回调处理
     */
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        // TODO Auto-generated method stub
        return null;
    }

}
