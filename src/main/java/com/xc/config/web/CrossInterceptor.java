package com.xc.config.web;

import com.alibaba.druid.util.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Configuration
public class CrossInterceptor  implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String origin = request.getHeader(HttpHeaders.ORIGIN);
        if (!StringUtils.isEmpty(origin) || !"null".equals(origin)) {
            //设置跨域的站点
            response.setHeader("Access-Control-Allow-Origin", origin);
            //设置跨域携带认证信息 例如cookies
            response.setHeader("Access-Control-Allow-Credentials", "false");
            //设置跨域请求方法
            response.setHeader("Access-Control-Allow-Methods", "*");
            //设置跨域请求头信息
            response.setHeader("Access-Control-Allow-Headers", "authentication");
            //设置跨域时间
            response.setHeader("Access-Control-Max-Age", "3600");
        }
        return true;
    }

}
