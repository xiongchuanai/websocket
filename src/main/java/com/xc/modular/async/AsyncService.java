package com.xc.modular.async;

import com.xc.core.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import java.io.File;

/**
 * @program: guns
 * @author: XIONG CHUAN
 * @create: 2019-06-10 20:10
 * @description: 异步服务入口
 **/
@Slf4j
@EnableAsync
@Component
public class AsyncService {

    /**
     * 异步方法
     */
    @Async
    public void async() {

    }

    /**
     * 删除指定文件夹下所有文件
     *
     * @param path 文件夹完整绝对路径
     */
    @Async
    public void delAllFile(String path) {

        DateUtils.countDown(10, "删除文件", false);

        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return;
        }
        if (!file.isDirectory()) {
            return;
        }
        String[] tempList = file.list();
        File temp = null;
        for (int i = 0; i < tempList.length; i++) {
            if (path.endsWith(File.separator)) {
                temp = new File(path + tempList[i]);
            } else {
                temp = new File(path + File.separator + tempList[i]);
            }
            if (temp.isFile()) {
                temp.delete();
            }
            if (temp.isDirectory()) {
                //先删除文件夹里面的文件
                delAllFile(path + "/" + tempList[i]);
                //再删除空文件夹
                //delFolder(path + "/" + tempList[i]);
                flag = true;
            }
        }
        log.info("删除完成！");
        //return flag;
    }
}
