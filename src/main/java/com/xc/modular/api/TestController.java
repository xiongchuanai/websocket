/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xc.modular.api;

import cn.stylefeng.roses.core.reqres.response.ResponseData;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 测试
 */
@RestController
@RequestMapping("/api/test")
public class TestController {

    @PostMapping(value = "/test", produces = {"application/json;charset=UTF-8"})
    public ResponseData payOrder(@ApiParam(value = "字段", required = true)
                                 @RequestParam(value = "param") String param,
                                 HttpServletRequest request, HttpServletResponse response) {

        return ResponseData.success();
    }

    @PostMapping(value = "/mqtt")
    public void test(HttpServletRequest request, HttpServletResponse response) {

        String clientId = request.getParameter("clientid");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        System.out.println("clientId：" + clientId);
        System.out.println("username：" + username);
        System.out.println("clientId：" + password);

        try {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println("ERROR");
        } catch (Exception e) {
        }
        //return ResponseData.success();
    }
}
