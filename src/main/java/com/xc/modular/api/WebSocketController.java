package com.xc.modular.api;

import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.xc.core.page.LayuiPageFactory;
import com.xc.modular.service.WebSocketService;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Slf4j
@Controller
@RequestMapping("/websocket")
public class WebSocketController {

    private String PREFIX = "/modular/websocket/";

    @Resource
    private WebSocketService webSocketService;

    /**
     * 跳转首页
     */
    @RequestMapping("")
    public String index() {
        log.info("访问websocket页面【websocket.html】");
        return PREFIX + "websocket.html";

    }

    /**
     * 跳到传输框
     */
    @RequestMapping("/send")
    public String channelSend(@RequestParam(required = false) String sessionId,
                              @RequestParam(required = false) String value,
                              Model model) {
        HashMap<String, String> hash = new HashMap<>(2);
        hash.put("sessionId", sessionId);
        hash.put("value", StringUtils.substringBeforeLast(value, ","));
        model.addAllAttributes(hash);
        return PREFIX + "websocketSend.html";
    }

    /**
     * 跳转到发送数据详情页面
     */
    @RequestMapping("/info")
    public String sendInfo(@RequestParam(required = false) String sessionId, Model model) {
        HashMap<String, String> hash = new HashMap<>();
        hash.put("sessionId", sessionId);
        model.addAllAttributes(hash);
        return PREFIX + "websocketSendInfo.html";
    }

    /**
     * 列表
     */
    @ResponseBody
    @RequestMapping(value = "/list")
    public Object list() {
        return LayuiPageFactory.createPageInfo(webSocketService.selectAll());
    }

    @RequestMapping(value = "/send/message")
    @ResponseBody
    public ResponseData send(@ApiParam(name = "sessionId", value = "连接session的id", required = true)
                             @RequestParam(value = "sessionId") String sessionId,
                             @ApiParam(name = "value", value = "小程序 或者 数据大屏key", required = true)
                             @RequestParam(value = "value") String value,
                             @ApiParam(name = "success", value = "调用者数据处理是否成功", required = true)
                             @RequestParam(value = "success") String success,
                             @ApiParam(name = "exceptionMessage", value = "异常消息，如果success为false，就不为空")
                             @RequestParam(value = "exceptionMessage", required = false) String exceptionMessage,
                             HttpServletRequest request) {
        return webSocketService.send(request, sessionId, value, success, exceptionMessage);
    }

    /**
     * 获取发送数据详情
     */
    @RequestMapping(value = "/send/info")
    @ResponseBody
    public Object sendInfo(@RequestParam(required = false) String sessionId) {

        return LayuiPageFactory.createPageInfo(webSocketService.sendInfo(sessionId));
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public ResponseData delete(@RequestParam String sessionId) {
        webSocketService.delete(sessionId);
        return ResponseData.success();
    }
}


