package com.xc.modular.api;

import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.xc.modular.service.ApiCommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Api(description = "发送消息")
@RestController
@RequestMapping("/api/websocket")
public class ApiCommonController {

    @Resource
    private ApiCommonService apiCommonService;

    @ApiOperation(value = "推送消息给前端", notes = "发送websocket消息", httpMethod = "POST")
    @PostMapping(value = "/push")
    public ResponseData push(@ApiParam(name = "key", value = "小程序 或者 数据大屏key", required = true)
                             @RequestParam(value = "key") String key,
                             @ApiParam(name = "jsonStr", value = "是否成功的json字符串", required = true)
                             @RequestParam(value = "jsonStr") String jsonStr,
                             HttpServletRequest request) {
        return apiCommonService.push(request, key, jsonStr);
    }

    @ApiOperation(value = "获取websocket的连接信息", notes = "获取websocket的连接信息", httpMethod = "POST")
    @PostMapping(value = "/get/linkInfo")
    public ResponseData linkInfo() {
        return apiCommonService.linkInfo();
    }
}
