package com.xc.modular.service;

import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.core.util.ToolUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xc.websocket.util.WsData;
import com.xc.websocket.util.WsServerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Service
public class ApiCommonService {

    /**
     * @return cn.stylefeng.roses.core.reqres.response.ResponseData
     * @Author xiongchuan
     * @Description 通过value发送数据  socket推送数据
     * @Date 2020/6/7 10:16
     * @Param [request, value, jsonStr]
     **/
    public ResponseData push(HttpServletRequest request, String value, String jsonStr) {
        log.info("接收到推送请求 key：{}   json：{}", value, jsonStr);
        WsData wsData = assembleData(value, jsonStr);
        WsServerUtil.sendByValue(value, wsData);
        return ResponseData.success();
    }

    /**
     * @return cn.stylefeng.roses.core.reqres.response.ResponseData
     * @Author xiongchuan
     * @Description 通过sessionId发送数据 后台管理推送数据
     * @Date 2020/6/7 10:16
     * @Param [sessionId, value, success, exceptionMessage]
     **/
    public ResponseData send(String sessionId, String value, String success, String exceptionMessage) {
        log.info("测试websocket数据传输 key：{}   message：{}  success：{}", value, exceptionMessage, success);
        WsData wsData = assembleData(value, success, exceptionMessage, null, null);
        WsServerUtil.send(sessionId, wsData);
        return ResponseData.success();
    }

    /**
     * @return cn.stylefeng.roses.core.reqres.response.ResponseData
     * @Author xiongchuan
     * @Description 获取websocket的连接信息
     * @Date 2020/6/7 10:16
     **/
    public ResponseData linkInfo() {
        return ResponseData.success(WsServerUtil.getSessionValueAll());
    }

    //组装数据
    public WsData assembleData(String value, String jsonStr) {
        JSONObject json = JSON.parseObject(jsonStr);
        return assembleData(value, json.getString("success"), json.getString("message"),
                json.getString("result"), json.getString("type"));
    }

    public WsData assembleData(String value, String success, String exceptionMessage, String result, String type) {
        WsData wsData = new WsData();
        if (!ToolUtil.isOneEmpty(value)) {
            String[] split = value.split(",");
            JSONObject json = new JSONObject();
            for (int i = 1; i <= split.length; i++) {
                json.put("param" + i, split[i - 1]);
            }
            wsData = JSON.parseObject(json.toJSONString(), wsData.getClass());
        }
        if ("true".equals(success)) {
            wsData.setSuccess(true);
        } else {
            wsData.setSuccess(false);
        }
        wsData.setMessage(exceptionMessage == null ? null : exceptionMessage);
        wsData.setResult(result == null ? null : result);
        wsData.setType(type == null ? null : Integer.valueOf(type));
        return wsData;
    }

    public static void main(String[] args) {

        new ApiCommonService().assembleData("1111,2222", null, null, null, null);

    }
}
