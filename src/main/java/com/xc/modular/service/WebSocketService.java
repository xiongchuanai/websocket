package com.xc.modular.service;

import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xc.constant.ChargeInfoEnum;
import com.xc.core.util.DateUtils;
import com.xc.websocket.util.WsServerUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Service
public class WebSocketService {

    @Resource
    private ApiCommonService apiCommonService;

    /**
     * 列表
     */
    public Page selectAll() {
        List<Session> sessionList = WsServerUtil.getSessionAll();
        //log.info("获取websocket的连接详情，总连接数：{}", sessionList.size());
        List<HashMap<String, Object>> list = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(sessionList)) {
            sessionList.forEach(session -> {
                HashMap<String, Object> messageList = WsServerUtil.getMessage(session);
                InetSocketAddress addr = WsServerUtil.getRemoteAddress(session);
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("sessionId", session.getId());
                hashMap.put("value", WsServerUtil.getValueBySession(session));
                hashMap.put("ip", addr.getAddress());
                hashMap.put("port", addr.getPort());
                hashMap.put("explain", explain(WsServerUtil.getValueBySession(session)));
                String time = WsServerUtil.getLinkTime(session);
                hashMap.put("time", time);
                hashMap.put("onLineTime", DateUtils.getBetweenMinutes(time, DateUtils.getDateTime()));
                hashMap.put("messageNum", messageList.size());
                list.add(hashMap);
            });
        }
        List<HashMap<String, Object>> collect = list.stream().sorted(Comparator.comparing(WebSocketService::communicationTime).reversed()).collect(Collectors.toList());
        Page pageInfo = new Page();
        pageInfo.setRecords(collect);
        return pageInfo;
    }

    // 根据时间排序
    private static Date communicationTime(Map<String, Object> map) {
        return DateUtils.formatStringToDate((String) map.get("time"), "yyyy-MM-dd HH:mm:ss");
    }

    // 获取说明
    private static String explain(String value) {
        String s = null;
        String[] split = value.split(",");
        if (split.length >= 3) {
            ChargeInfoEnum chargeInfoEnum = ChargeInfoEnum.getEnum(split[2]);
            if (!Objects.equals("unknown", chargeInfoEnum.getKey())) {
                s = chargeInfoEnum.getMessage();
            }
        }

        // 电单车数据大屏
        if (split.length == 2) {
            ChargeInfoEnum chargeInfoEnum = ChargeInfoEnum.getEnum(split[0]);
            if (!Objects.equals("unknown", chargeInfoEnum.getKey())) {
                s = chargeInfoEnum.getMessage();
            }
        }

        if (StringUtils.isBlank(s)) {
            String newValue = value.substring(value.lastIndexOf(",")).replace(",", "");
            if (StringUtils.isNotBlank(newValue)) s = ChargeInfoEnum.getEnum(newValue).getMessage();
        }
        return s;
    }

    /**
     * @return cn.stylefeng.roses.core.reqres.response.ResponseData
     * @Author xiongchuan
     * @Description 数据传输
     * @Date 2020/6/7 10:18
     * @Param [request, sessionId, value, success, exceptionMessage, startStep, startSuccess]
     **/
    public ResponseData send(HttpServletRequest request, String sessionId, String value, String success, String exceptionMessage) {
        return apiCommonService.send(sessionId, value, success, exceptionMessage);
    }

    public void delete(String sessionId) {
        WsServerUtil.remove(sessionId);
    }

    /**
     * @return com.baomidou.mybatisplus.core.metadata.IPage
     * @Author xiongchuan
     * @Description 获取发送数据详情
     * @Date 2020/6/7 11:28
     * @Param [sessionId]
     **/
    public IPage sendInfo(String sessionId) {
        Session session = WsServerUtil.getBySessionId(sessionId);
        HashMap<String, Object> map = WsServerUtil.getSessionInfo(session);
        List<HashMap<String, Object>> list = new ArrayList<>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            //消息发送时间
            String time = entry.getKey();
            //消息内容
            HashMap<String, Object> mapInfo = (HashMap<String, Object>) entry.getValue();
            //最终返回的数据
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("value", mapInfo.get("value"));
            hashMap.put("data", mapInfo.get("message"));
            hashMap.put("time", time);
            hashMap.put("isSend", ((Boolean) mapInfo.get("success")) ? "成功" : "失败");
            list.add(hashMap);
        }
        Page pageInfo = new Page();
        pageInfo.setRecords(list);
        return pageInfo;
    }

    /**
     * 三小时
     */
    private static int TIME = 180;

    /**
     *  停掉在线时长超过一天的连接
     */
    public static int killExpires() {
        AtomicInteger num = new AtomicInteger();
        List<Session> sessionList = WsServerUtil.getSessionAll();
        sessionList.stream().forEach(session ->{
            // 计算在线时常
            int time = DateUtils.getBetweenMinutes(WsServerUtil.getLinkTime(session), DateUtils.getDateTime());
            if (time >= TIME) {
                WsServerUtil.remove(session);
                num.getAndIncrement();
            }
        });
        return num.get();
    }
}
