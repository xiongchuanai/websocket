package com.xc.taskTimer;

import com.xc.core.util.DateUtils;
import com.xc.modular.service.WebSocketService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务
 **/
@Slf4j
@Component
@EnableScheduling
public class JobOrderRefund {

    @Value("${websocket.scheduling}")
    private boolean scheduling;

    /**
     *  定时每小时清理过期的连接通道
     *
     *  "0/10 * * * * ?"  每10秒
     */
    @Scheduled(cron="0 0 */1 * * ?")
    public void testRefund() {
        if (scheduling) return;
        int num = WebSocketService.killExpires();
        log.info("执行停用过期连接完成，停用通道数：" + num + "\t当前时间：" + DateUtils.getDateTime());
    }

}
