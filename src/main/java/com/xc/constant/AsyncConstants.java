package com.xc.constant;

/**
 * @program: guns
 * @author: XIONG CHUAN
 * @create: 2019-06-11 11:40
 * @description: 异步常量
 **/

public class AsyncConstants {

    //核心线程数量
    public static int CORE_THREAD_NUM = 10;
    //最大线程数量
    public static int MAX_THREAD_NUM = 100;
    //维护线程允许的空闲时间 单位 秒
    public static int KEEP_ALIVE_SECONDS = 30;
    //缓存队列数量
    public static int CACHE_NUM = 9999;
    //线程名前缀,方便排查问题
    public static String PREFIX_NAME = "order-Refund-thread-";
}
