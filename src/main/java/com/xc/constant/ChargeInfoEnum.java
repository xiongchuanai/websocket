package com.xc.constant;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Objects;

@Slf4j
public enum ChargeInfoEnum {

    CHARGE_START("chargeStart", "开启充电", "enabled"),
    CHARGE_STOP("chargeStop", "结束充电", "enabled"),
    CHARGE_PULL_GUN("pullGun", "提示拔枪", "enabled"),
    CHARGE_RECORD("chargeRecord", "充电记录实时更新", "enabled"),
    CHARGE_STATION("chargeStation", "充电桩状态更新", "enabled"),
    CHARGE_INTERFACE("chargeInterface", "充电枪状态更新", "enabled"),
    CHARGE_CURRENT_RECORD("chargeCurrentRecord", "本次记录上报", "disabled"),
    CHARGE_TEST("chargeTest", "测试websocket", "enabled"),
    UNKNOWN("unknown", "未知", "enabled"),
    ECYCLE("ecycleWebsocket", "小程序", "enabled"),
    DATA_SCREEN("ecycle_dateScreen", "大屏动态数据", "enabled");

    private String key;
    private String message;
    private String status;

    ChargeInfoEnum(String key, String message, String status) {
        this.key = key;
        this.message = message;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 根据key获取枚举的属性值
     *
     * @param key
     * @return
     */
    public static String getEnumType(String key) {
        ChargeInfoEnum infoEnum = Arrays.asList(ChargeInfoEnum.values()).stream()
                .filter(chargeInfoEnum -> chargeInfoEnum.getKey().equals(key))
                .findFirst().orElse(ChargeInfoEnum.UNKNOWN);
        String result = null;
        if (infoEnum != null) {
            result = infoEnum.getMessage() + ("enabled".equals(infoEnum.getStatus()) ? " --- 启用" : " --- 停用");
        }
        return result;
    }

    /**
     * 根据key获取枚举
     *
     * @param key
     * @return
     */
    public static ChargeInfoEnum getEnum(String key) {
        return Arrays.asList(ChargeInfoEnum.values()).stream()
                .filter(chargeInfoEnum -> chargeInfoEnum.getKey().equals(key))
                .findFirst().orElse(ChargeInfoEnum.UNKNOWN);
    }

    /**
     * 校验充电桩value是否可用
     */
    public static boolean chargeCheckValue(String value) {
        boolean b = true;
        String[] s = value.split(",");
        if (s.length != 4) {
            b = false;
        } else {
            if (s.length >= 3) {
                ChargeInfoEnum chargeInfoEnum = ChargeInfoEnum.getEnum(s[2]);
                if (chargeInfoEnum.getKey().equals(ChargeInfoEnum.ECYCLE.getKey())) {
                    b = false;
                }
                if (Objects.equals("disabled", chargeInfoEnum.getStatus())) {
                    b = false;
                    log.info("webSocket连接的value已停用, 请检查当前value：" + value + "\n");
                }
                if (Objects.equals("unknown", chargeInfoEnum.getKey())) {
                    b = false;
                    log.info("webSocket连接的value未知, 请检查当前value：" + value + "\n");
                }
            }
        }
        return b;
    }

    /**
     * 校验电单车value是否可用
     */
    public static boolean ecycleCheckValue(String value) {
        boolean b = true;
        String s = value.substring(value.lastIndexOf(",")).replace(",", "");
        if (StringUtils.isNotBlank(s)) {
            ChargeInfoEnum chargeInfoEnum = ChargeInfoEnum.getEnum(s);
            if (chargeInfoEnum != null &&
                    !Objects.equals("unknown", chargeInfoEnum.getKey()) &&
                    !Objects.equals("disabled", chargeInfoEnum.getStatus())) {

            } else {
             b = false;
            }
        }
        return b;
    }

    /**
     * 校验电单车平台数据大屏value是否可用
     * value："ecycle_dateScreen"+"商户号"
     */
    public static boolean dateScreenCheckValue(String value) {
        boolean b = true;
        if (StringUtils.isBlank(value)) return false;
        String[] s = value.split(",");
        if (StringUtils.isNotBlank(s[0])) {
            ChargeInfoEnum chargeInfoEnum = ChargeInfoEnum.getEnum(s[0]);
            if (chargeInfoEnum != null &&
                    !Objects.equals("unknown", chargeInfoEnum.getKey()) &&
                    !Objects.equals("disabled", chargeInfoEnum.getStatus())) {
            } else {
                b = false;
            }
        }
        return b;
    }
}
