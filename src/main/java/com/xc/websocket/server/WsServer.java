package com.xc.websocket.server;

import com.xc.constant.ChargeInfoEnum;
import com.xc.websocket.util.WsServerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

@Slf4j
@Component
@ServerEndpoint(value = "/chargeWebsocket")
public class WsServer {

    /**
     * 连接时执行
     */
    @OnOpen
    public void onOpen(Session session) {
        // ws连接的时候触发的代码，onOpen中我们不做任何操作
        log.info("有活跃连接：" + session.toString());
    }

    /**
     * 关闭时执行
     */
    @OnClose
    public void onClose(Session session) {
        log.info("连接：" + session.toString() + "关闭");
        WsServerUtil.remove(session);
    }

    /**
     * 收到消息时执行
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        log.info("收到消息：{}", message);
        join(session, message);
    }

    /**
     * 连接错误时执行
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.info("通道： " + session.toString() + " 的连接错误：" + error.getMessage());
    }

    /**
     * 将websocket加入连接池
     */
    private void join(Session session, String value) {
        if (ChargeInfoEnum.chargeCheckValue(value)) {
            log.info("充电桩 连接websocket， 连接的value：{}", value);
            WsServerUtil.chargeAdd(session, value);
        } else if (ChargeInfoEnum.ecycleCheckValue(value)) {
            log.info("电单车小程序 连接websocket， 连接的value：{}", value);
            WsServerUtil.ecycleAdd(session, value);
        } else if (ChargeInfoEnum.dateScreenCheckValue(value)) {
            log.info("电单车数据大屏动态数据 连接websocket， 连接的value：{}", value);
            WsServerUtil.dateScreenAdd(session, value);
        } else {
            log.info("无效的websocket连接，value：{}", value);
        }
    }
}
