package com.xc.websocket.server;

import javax.websocket.*;
import java.util.concurrent.Future;

@ClientEndpoint
public class MyClient {

    private Session session;

    @OnOpen
    public void open(Session session) {
        this.session = session;
    }

    @OnMessage
    public void onMessage(String message) {
        System.out.println("onMessage：" + message);
    }

    @OnClose
    public void onClose() {
        System.out.println("onClose：");
    }

    /**
     * 发送客户端消息到服务端
     *
     * @param message 消息内容
     */
    public void send(String message) {

        Future<Void> voidFuture = this.session.getAsyncRemote().sendText(message);
    }
}
