package com.xc.websocket.util;

import com.alibaba.fastjson.JSON;
import com.xc.constant.ChargeInfoEnum;
import com.xc.core.util.DateUtils;

import javax.websocket.RemoteEndpoint.Async;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import javax.websocket.Session;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class WsServerUtil {

    /**
     * TIME    ：连接时间
     * WECHAT  ：自定义特殊字符，用来定位微信页面
     * DATA_SCREEN  ：自定义特殊字符，电单车后台前端页面
     * INFO    ：保存推送消息
     */
    private static String TIME = "time";
    private static String WECHAT = "wechat";
    private static String DATA_SCREEN = "dateScreen";
    private static String INFO = "info";

    /**
     * 初始容量 2的10次幂
     */
    private static Integer capacity = 2 ^ 10;
    /**
     * 保存session：value
     */
    private static final ConcurrentHashMap<Session, String> SESSION_MAP = new ConcurrentHashMap<Session, String>(capacity);
    /**
     * 保存value：session
     */
    private static final ConcurrentHashMap<String, Session> VALUE_SESSION_MAP = new ConcurrentHashMap<String, Session>(capacity);
    /**
     * 保存sessionId：session
     */
    private static final ConcurrentHashMap<String, Session> SESSION_ID_MAP = new ConcurrentHashMap<String, Session>(capacity);
    /**
     * 保存通讯详情
     */
    private static ConcurrentHashMap<Session, HashMap<String, Object>> SESSION_INFO_MAP = new ConcurrentHashMap<Session, HashMap<String, Object>>(capacity);

    /**
     * 获取连接时间
     */
    public static String getLinkTime(Session session) {
        String time = null;
        if (SESSION_INFO_MAP.containsKey(session)) {
            time = (String) SESSION_INFO_MAP.get(session).get(WsServerUtil.TIME);
        }
        return time;
    }

    /**
     * 获取推送的消息
     */
    public static HashMap<String, Object> getMessage(Session session) {
        HashMap<String, Object> info = null;
        if (SESSION_INFO_MAP.containsKey(session)) {
            info = (HashMap<String, Object>) SESSION_INFO_MAP.get(session).get(WsServerUtil.INFO);
        }
        return info;
    }

    /**
     * 获取推送的详情，包括每一条数据的推送时间，value
     */
    public static HashMap<String, Object> getSessionInfo(Session session) {
        HashMap<String, Object> info = null;
        if (SESSION_INFO_MAP.containsKey(session)) {
            info = (HashMap<String, Object>) SESSION_INFO_MAP.get(session).get(WsServerUtil.INFO);
        }
        return info;
    }

    /**
     * 获取所有的session
     */
    public static List<Session> getSessionAll() {
        Set<Session> set = SESSION_MAP.keySet();
        return new ArrayList<>(set);
    }

    /**
     * 获取所有session对应的值
     */
    public static List<String> getSessionValueAll() {
        Collection<String> values = SESSION_MAP.values();
        return new ArrayList<>(values);
    }

    /**
     * 获取session的完整value
     */
    public static String getValueBySession(Session session) {
        String value = null;
        if (SESSION_MAP.containsKey(session) && SESSION_INFO_MAP.containsKey(session)) {
            value = SESSION_MAP.get(session);
            if (checkParam4(session.getId())) {
                value = value + "," + SESSION_INFO_MAP.get(session).get(WsServerUtil.WECHAT);
            }
        }
        return value;
    }

    /**
     * 通过sessionId获取session
     */
    public static Session getBySessionId(String sessionId) {
        return SESSION_ID_MAP.get(sessionId);
    }

    /**
     * 获取WebSocket,
     */
    public static List<Session> getByValue(String value) {
        List<Session> list = new ArrayList<>();
        for (Session session : SESSION_MAP.keySet()) {
            String tempValue = SESSION_MAP.get(session);
            if (tempValue.equals(value)) {
                list.add(session);
            }
        }
        return list;
    }

    /**
     * 充电桩添加连接
     */
    public static void chargeAdd(Session session, String value) {
        //对应SESSION_MAP
        String value1 = StringUtils.substringBeforeLast(value, ",");
        //对应SESSION_INFO_MAP
        String value2 = value.substring(value.lastIndexOf(",")).replace(",", "");
        HashMap<String, Object> infoMap = createInfoMap(session, value2, WsServerUtil.WECHAT);
        add(session, value1, infoMap, WsServerUtil.WECHAT);
    }

    /**
     * 电单车小程序添加连接
     */
    public static void ecycleAdd(Session session, String value) {
        //对应SESSION_MAP
        int index = value.lastIndexOf(",", value.lastIndexOf(",") - 1);
        String value1 = value.substring(0, index);
        //对应SESSION_INFO_MAP
        String value2 = value.substring(index + 1);

        HashMap<String, Object> infoMap = createInfoMap(session, value2, WsServerUtil.WECHAT);
        add(session, value1, infoMap, WsServerUtil.WECHAT);
    }

    /**
     * 电单车数据大屏动态数据添加连接
     */
    public static void dateScreenAdd(Session session, String value) {

        HashMap<String, Object> infoMap = createInfoMap(session, value, WsServerUtil.DATA_SCREEN);
        add(session, value, infoMap, WsServerUtil.DATA_SCREEN);
    }

    public static HashMap<String, Object> createInfoMap(Session session, String value, String location) {
        HashMap<String, Object> infoMap = new HashMap<>();
        if (SESSION_INFO_MAP.containsKey(session)) {
            //连接时间
            infoMap.put(WsServerUtil.TIME, getLinkTime(session));
        } else {
            infoMap.put(WsServerUtil.TIME, DateUtils.getDateTime());
        }

        if (StringUtils.isNotBlank(value)) {
            infoMap.put(location, value);
        }

        if (getMessage(session) != null) {
            infoMap.put(WsServerUtil.INFO, getMessage(session));
        } else {
            infoMap.put(WsServerUtil.INFO, new HashMap<>());
        }

        return infoMap;
    }

    /**
     * 添加连接
     */
    public static void add(Session session, String value, HashMap<String, Object> infoMap, String location) {
        SESSION_MAP.put(session, value);
        SESSION_INFO_MAP.put(session, infoMap);
        SESSION_ID_MAP.put(session.getId(), session);
        if (StringUtils.equals(location, WsServerUtil.DATA_SCREEN)) {
            // 电单车平台前端链接
            VALUE_SESSION_MAP.put(value, session);
        }
        log.info("活跃连接数：" + SESSION_MAP.size() + "\n");
    }

    /**
     * 移除连接
     */
    public static boolean remove(Session session) {
        boolean b = true;
        try {
            if (SESSION_MAP.containsKey(session)) {
                VALUE_SESSION_MAP.remove(SESSION_MAP.get(session));
                SESSION_MAP.remove(session);
                SESSION_INFO_MAP.remove(session);
                SESSION_ID_MAP.remove(session.getId());
                session.close();
                log.info("活跃连接数：" + SESSION_MAP.size() + "\n");
            } else {
                b = false;
            }
        } catch (Exception e) {
            b = false;
            log.info("删除活跃连接异常：" + e.getMessage() + "\n");
        }
        return b;
    }

    /**
     * 通过sessionId移除连接
     */
    public static boolean remove(String sessionId) {
        return remove(SESSION_ID_MAP.get(sessionId));
    }

    /**
     * 记录发送的消息
     * param boolean  表示消息发送是否成功
     */
    private static void recordMessage(Session session, String message, boolean bool) {
        HashMap<String, Object> info = (HashMap<String, Object>) SESSION_INFO_MAP.get(session).get(WsServerUtil.INFO);
        HashMap<String, Object> messageMap = new HashMap<>();
        messageMap.put("success", bool);
        messageMap.put("message", message);
        messageMap.put("value", getValueBySession(session));
        info.put(DateUtils.getDateTime(), messageMap);
    }

    /**
     * 向特定的链接发送数据
     *
     * @param message
     */
    public static void send(Session session, String message) {
        if (null != session) {
            try {
                session.getBasicRemote().sendText(message);
                recordMessage(session, message, true);
            } catch (IOException e) {
                recordMessage(session, message, false);
                log.info("sessionId为" + session.getId() + "连接发送消息失败：" + e.getMessage() + "\n");
            }
        }
    }

    public static void send(String sessionId, String message) {
        Session session = getBySessionId(sessionId);
        send(session, message);
    }

    public static void send(List<Session> list, WsData data) {
        if (!CollectionUtils.isEmpty(list)) {
            list.stream().forEach(session -> {
                send(session.getId(), data);
            });
        }
    }

    public static void send(String sessionId, WsData data) {
        if (checkParam4(sessionId)) {
            String s = SESSION_INFO_MAP.get(SESSION_ID_MAP.get(sessionId)).get(WsServerUtil.WECHAT).toString();
            if (s.contains(",")) {
                data.setParam4(s.substring(0, s.lastIndexOf(",")));
            } else {
                data.setParam4(s);
            }
        }
        send(sessionId, JSON.toJSONString(data));
    }

    // 判断是否有param4
    public static boolean checkParam4(String sessionId) {
        Object o = SESSION_INFO_MAP.get(SESSION_ID_MAP.get(sessionId)).get(WsServerUtil.WECHAT);
        return o == null ? false : true;
    }

    /**
     * 向此value对应的所有链接发送数据
     *
     * @param value        通道映射的值，组成规则
     *                     a 充电桩发送充电启动完成帧接口  pileCode,recordCode,enum
     *                     b 充电桩发送充电停止完成帧接口  pileCode,recordCode,enum
     *                     c 充电记录实时更新            pileCode,recordCode,enum
     *                     d 站点详情                   deviceNo,accounts,enum
     *                     e 点击扫码，枪状态更新	  	    pileCode,orderNo,enum
     *                     f 本次记录上报                pileCode,recordCode,enum
     * @param data，需要发送的数据
     */
    public static void sendByValue(String value, WsData data) {
        Session session = VALUE_SESSION_MAP.get(value);
        if (null != session) {
            // 推送消息到电单车平台前端
            send(session, JSON.toJSONString(data));
            System.out.println("==========直接推送电单车前段消息==========");
            return;
        }
        //通过value获取session
        List<Session> list = getByValue(value);

        if (ChargeInfoEnum.CHARGE_STOP.getKey().equals(data.getParam3())) {
            if (CollectionUtils.isEmpty(list)) {
                String newValue = data.getParam1() + "," + data.getParam2() + "," + ChargeInfoEnum.CHARGE_RECORD.getKey();
                list = getByValue(newValue);
            }
        }
        send(list, data);
    }

    /**
     * 消息广播
     *
     * @param data
     */
    public static void broadcast(WsData data) {
        Set<Session> set = SESSION_MAP.keySet();
        send(new ArrayList<>(set), data);
    }

    /**
     * 获取客户端ip
     */
    public static InetSocketAddress getRemoteAddress(Session session) {
        if (session == null) {
            return null;
        }
        Async async = session.getAsyncRemote();

        //在Tomcat 8.0.x版本有效
//		InetSocketAddress addr = (InetSocketAddress) getFieldInstance(async,"base#sos#socketWrapper#socket#sc#remoteAddress");
        //在Tomcat 8.5以上版本有效
        InetSocketAddress addr = (InetSocketAddress) getFieldInstance(async, "base#socketWrapper#socket#sc#remoteAddress");
        return addr;
    }

    private static Object getFieldInstance(Object obj, String fieldPath) {
        String fields[] = fieldPath.split("#");
        for (String field : fields) {
            obj = getField(obj, obj.getClass(), field);
            if (obj == null) {
                return null;
            }
        }
        return obj;
    }

    private static Object getField(Object obj, Class<?> clazz, String fieldName) {
        for (; clazz != Object.class; clazz = clazz.getSuperclass()) {
            try {
                Field field;
                field = clazz.getDeclaredField(fieldName);
                field.setAccessible(true);
                return field.get(obj);
            } catch (Exception e) {
            }
        }
        return null;
    }
}
