package com.xc.core.util;/**
 * Created by fengchu on 16/5/28.
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.map.LinkedMap;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author xiongchuan
 * @date 16/5/28
 */
public class JsonUtils {
    /**
     * 单位缩进字符串。
     */
    private static String SPACE = "   ";

    /**
     * 返回格式化JSON字符串。
     *
     * @param json 未格式化的JSON字符串。
     * @return 格式化的JSON字符串。
     */
    public static String formatJson(String json) {
        StringBuffer result = new StringBuffer();

        int length = json.length();
        int number = 0;
        char key = 0;

        // 遍历输入字符串。
        for (int i = 0; i < length; i++) {
            // 1、获取当前字符。
            key = json.charAt(i);

            // 2、如果当前字符是前方括号、前花括号做如下处理：
            if ((key == '[') || (key == '{')) {
                // （1）如果前面还有字符，并且字符为“：”，打印：换行和缩进字符字符串。
                if ((i - 1 > 0) && (json.charAt(i - 1) == ':')) {
                    result.append('\n');
                    result.append(indent(number));
                }

                // （2）打印：当前字符。
                result.append(key);

                // （3）前方括号、前花括号，的后面必须换行。打印：换行。
                result.append('\n');

                // （4）每出现一次前方括号、前花括号；缩进次数增加一次。打印：新行缩进。
                number++;
                result.append(indent(number));

                // （5）进行下一次循环。
                continue;
            }

            // 3、如果当前字符是后方括号、后花括号做如下处理：
            if ((key == ']') || (key == '}')) {
                // （1）后方括号、后花括号，的前面必须换行。打印：换行。
                result.append('\n');

                // （2）每出现一次后方括号、后花括号；缩进次数减少一次。打印：缩进。
                number--;
                result.append(indent(number));

                // （3）打印：当前字符。
                result.append(key);

                // （4）如果当前字符后面还有字符，并且字符不为“，”，打印：换行。
                if (((i + 1) < length) && (json.charAt(i + 1) != ',')) {
                    result.append('\n');
                }

                // （5）继续下一次循环。
                continue;
            }

            // 4、如果当前字符是逗号。逗号后面换行，并缩进，不改变缩进次数。
            if ((key == ',')) {
                result.append(key);
                result.append('\n');
                result.append(indent(number));
                continue;
            }

            // 5、打印：当前字符。
            result.append(key);
        }

        return result.toString();
    }

    /**
     * 返回指定次数的缩进字符串。每一次缩进三个空格，即SPACE。
     *
     * @param number 缩进次数。
     * @return 指定缩进次数的字符串。
     */
    private static String indent(int number) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < number; i++) {
            result.append(SPACE);
        }
        return result.toString();
    }

    public static JSONObject requestToJson(HttpServletRequest request) {
        JSONObject jsonObject = new JSONObject(new LinkedMap());
        Map<?, ?> parameterMap = request.getParameterMap();
        Iterator<?> it = parameterMap.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            String[] values = (String[]) parameterMap.get(key);
            jsonObject.put(key, values[0]);
        }
        return jsonObject;
    }

    public static HashMap<String, Object> toHashMap(Object object) {

        if (object == null) {
            return null;
        }

        HashMap<String, Object> map = new HashMap<>();

        Field[] declaredFields = object.getClass().getDeclaredFields();

        if (object.getClass().getSuperclass() != null) {
            declaredFields = concatenateArrays(declaredFields, object.getClass().getSuperclass().getDeclaredFields());
        }

        for (Field field : declaredFields) {
            field.setAccessible(true);
            try {
                map.put(field.getName(), field.get(object));
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    public static JSONObject toJSONObject(Object object){
        String jsonStr = JSONObject.toJSONString(object);
        return JSON.parseObject(jsonStr);
    }

    public static JSONArray toJSONArray(Object object){
        String jsonStr = JSONObject.toJSONString(object);
        return JSON.parseArray(jsonStr);
    }

    /**
     * 将一个字符串数组的内容全部添加到另外一个数组中，并返回一个新数组。
     *
     * @param array1
     *            第一个数组
     * @param array2
     *            第二个数组
     * @return T[] 拼接后的新数组
     */
    public static <T> T[] concatenateArrays(T[] array1, T[] array2) {
        if (isEmpty(array1)) {
            return array2;
        }
        if (isEmpty(array2)) {
            return array1;
        }
        T[] resArray = (T[]) java.lang.reflect.Array.newInstance(
                array1[0].getClass(), array1.length + array2.length);
        System.arraycopy(array1, 0, resArray, 0, array1.length);
        System.arraycopy(array2, 0, resArray, array1.length, array2.length);
        return resArray;
    }

    /**
     * 功能：判断数组是不是空。（null或者length==0）
     *
     * @param array
     *            数组
     * @return boolean 空返回true，否则返回false。
     */
    public static <T> boolean isEmpty(T[] array) {
        return (array == null || array.length == 0);
    }

}
