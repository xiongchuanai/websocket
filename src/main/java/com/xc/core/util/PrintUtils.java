package com.xc.core.util;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;


public class PrintUtils {

    public static void print(Object obj) {
        if (obj instanceof List) {
            JSONArray jsonObject = JsonUtils.toJSONArray(obj);
            System.out.println(JsonUtils.formatJson(jsonObject.toString()));
        }if (obj instanceof Object[]) {
            JSONArray jsonObject = JsonUtils.toJSONArray(obj);
            System.out.println(JsonUtils.formatJson(jsonObject.toString()));
        } else if (obj instanceof String) {
            System.out.println(obj);
        } else if (obj instanceof Integer) {
            System.out.println(obj);
        } else if (obj instanceof Double) {
            System.out.println(obj);
        } else if (obj instanceof Float) {
            System.out.println(obj);
        } else if (obj instanceof Boolean) {
            System.out.println(obj);
        } else {
            JSONObject jsonObject = JsonUtils.toJSONObject(obj);
            System.out.println(JsonUtils.formatJson(jsonObject.toString()));
        }
    }

}
