package com.xc.core.util;

import cn.stylefeng.roses.core.util.ToolUtil;
import org.apache.commons.lang3.StringUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @program:
 * @author: XIONG CHUAN
 * @create: 2019-07-09 13:56
 * @description: MD5
 **/

public class MD5Str {

    /**
     * @param sourceStr 需要加密的字符串
     * @param num       位数
     * @author xiongchuan on 2019/7/9 13:57
     * @DESCRIPTION:
     * @return: java.lang.String
     */
    public static String MD5(String sourceStr, int num) {

        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(sourceStr.getBytes());
            byte b[] = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            result = buf.toString();
            switch (num) {
                case 32:
                    return result;
                case 16:
                    return buf.toString().substring(8, 24);
                default:
                    return null;
            }
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e);
        }
        return result;
    }

    /**
     * @param sourceStr 源字符串
     * @author xiongchuan on 2019/7/9 13:57
     * @DESCRIPTION:
     * @return: java.lang.String
     */
    public static String MD5ZeroFill(String sourceStr) {

        if (StringUtils.isEmpty(sourceStr)) return null;

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < sourceStr.length(); i++) {
            sb.append(sourceStr.charAt(i) + (i + 1 + ""));
        }
        return sb.toString();
    }

    /**
     * 获得md5加密后补充完整的的字符串
     *
     * @param sourceStr 源字符串
     * @param num       位数
     * @author xiongchuan on 2019/7/9 13:57
     * @DESCRIPTION:
     * @return: java.lang.String
     */
    public static String getStr(String sourceStr, int num) {

        if (ToolUtil.isOneEmpty(sourceStr, num)) return null;
        String str = MD5ZeroFill(MD5(sourceStr, num));

        return str;
    }
}
