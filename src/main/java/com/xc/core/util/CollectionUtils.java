package com.xc.core.util;

import cn.stylefeng.roses.core.util.ToolUtil;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionUtils {

    /**
     * 功能：以','切割字符串
     *
     * @param listStr 源字符串
     * @return List
     */
    public static List<String> listStrToList(String listStr) {

        if (ToolUtil.isEmpty(listStr)) {
            return new ArrayList<>();
        }
        listStr = listStr.replace("，", ",");

        return arrayToList(listStr.split(","))
                .stream()
                .filter(ToolUtil::isNotEmpty)
                .collect(Collectors.toList());
    }

    /**
     * 功能：将数组转为list。
     *
     * @param objs 源数组
     * @return List
     */
    public static <T> List<T> arrayToList(T[] objs) {
        if (isEmpty(objs)) {
            return null;
        }
        List<T> list = new LinkedList<T>();
        for (T obj : objs) {
            list.add(obj);
        }
        return list;
    }

    /**
     * 功能：判断数组是不是空。（null或者length==0）
     *
     * @param array 数组
     * @return boolean 空返回true，否则返回false。
     */
    public static <T> boolean isEmpty(T[] array) {
        return (array == null || array.length == 0);
    }
}
